package tv.extrememoderation

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import tv.extrememoderation.services.PointsService

@Slf4j
@SpringBootApplication
class PointsApplication implements CommandLineRunner {

	@Autowired
	PointsService pointsService

	static void main(String[] args) {
		SpringApplication.run PointsApplication, args
	}

	@Override
	void run(String... args) throws Exception {
		log.info 'Bootstrapping application...'

    /*
    log.info '--------------------------------------------------------------------------------'
    String username = 'exmo_test'
    def p = [
      (Math.random()*100+100) as long,
      (Math.random()*100+50) as long
    ]

    p << ((Math.random() * (p[0]+p[1])) as long)

    Points myTestPoints = pointsService.getPoints username
    log.info "Got points for $username, they currently have $myTestPoints.points points"

    myTestPoints = pointsService.addPoints username, p[0]
    log.info "Added ${p[0]} points to $username, they now have $myTestPoints.points points"

    myTestPoints = pointsService.subtractPoints username, p[1]
    log.info "Removed ${p[1]} points from $username, they now have $myTestPoints.points points"

    myTestPoints = pointsService.setPoints username, p[2]
    log.info "Set $username's points to ${p[2]}, they now have $myTestPoints.points points"
    log.info '--------------------------------------------------------------------------------'
    */
	}
}
