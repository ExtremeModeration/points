package tv.extrememoderation.model

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
 * @author <a href="http://stevegood.rocks">Steve Good</a>
 * @since 6/6/16.
 */
@Repository
interface PointsRepository extends CrudRepository<Points, UUID> {
  Points findByUsername(String username)
}
