package tv.extrememoderation.model

/**
 * @author <a href="http://stevegood.rocks">Steve Good</a>
 * @since 6/6/16.
 */
class PointsException extends Exception {
  static String NOT_ENOUGH_POINTS_TO_SUBTRACT = 'Not enough points to subtract'

  Points points
}
