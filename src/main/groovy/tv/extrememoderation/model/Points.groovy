package tv.extrememoderation.model

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * @author <a href="http://stevegood.rocks">Steve Good</a>
 * @since 6/6/16.
 */
@Entity
class Points {

  @Id
  @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
  @GeneratedValue(generator = "uuid-gen")
  @Type(type="pg-uuid")
  UUID id

  String username
  long points = 0

}
