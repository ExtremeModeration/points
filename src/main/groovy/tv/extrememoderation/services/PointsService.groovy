package tv.extrememoderation.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import tv.extrememoderation.model.Points
import tv.extrememoderation.model.PointsException
import tv.extrememoderation.model.PointsRepository

/**
 * @author <a href="http://stevegood.rocks">Steve Good</a>
 * @since 6/6/16.
 */
@Service
@Transactional
class PointsService {

  @Autowired
  PointsRepository pointsRepository

  Points getPoints(String username) {
    Points p = pointsRepository.findByUsername username
    if (!p) {
      p = new Points(username: username, points: 0)
      pointsRepository.save p
    }
    p
  }

  Points setPoints(String username, long points) {
    Points p = getPoints username
    p.points = points
    pointsRepository.save p
  }

  Points addPoints(String username, long points) {
    Points p = getPoints username
    p.points += points
    pointsRepository.save p
  }

  Points subtractPoints(String username, long points) {
    Points p = getPoints username

    if (p.points < points) {
      throw new PointsException(message: PointsException.NOT_ENOUGH_POINTS_TO_SUBTRACT, points: p)
    }

    p.points -= points
    pointsRepository.save p
  }

}
